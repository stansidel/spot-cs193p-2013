//
//  TagsTabBarController.h
//  SPoT
//
//  Created by stan on 21/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagsTabBarController : UITabBarController

- (void)transferSplitViewBarButtonItemToViewController:(id)destinationViewController;

@end
