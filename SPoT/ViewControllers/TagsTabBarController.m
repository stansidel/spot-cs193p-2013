//
//  TagsTabBarController.m
//  SPoT
//
//  Created by stan on 21/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "TagsTabBarController.h"
#import "PhotoViewController.h"

@interface TagsTabBarController() <UISplitViewControllerDelegate>
@end

@implementation TagsTabBarController

- (void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

- (id)splitViewDetailWithBarButtonItem
{
    id detail = [self.splitViewController.viewControllers lastObject];
    if (![detail respondsToSelector:@selector(setSplitViewBarButtonItem:)] ||
        ![detail respondsToSelector:@selector(splitViewBarButtonItem)]) detail = nil;
    return detail;
}

- (void)transferSplitViewBarButtonItemToViewController:(id)destinationViewController
{
    UIBarButtonItem *splitViewBarButtonItem = [[self splitViewDetailWithBarButtonItem] splitViewBarButtonItem];
    [[self splitViewDetailWithBarButtonItem] setSplitViewBarButtonItem:nil];
    if (splitViewBarButtonItem) {
        [destinationViewController setSplitViewBarButtonItem:splitViewBarButtonItem];
    }
}

#pragma mark - UISplitViewControllerDelegate

- (BOOL)splitViewController:(UISplitViewController *)svc
   shouldHideViewController:(UIViewController *)vc
              inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

- (void)splitViewController:(UISplitViewController *)svc
     willHideViewController:(UIViewController *)aViewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)pc
{
//    UIViewController *masterVC = [self selectedViewController];
//    barButtonItem.title = masterVC.title;
    barButtonItem.title = @"Photos List";
    
    id detailVC = [self.splitViewController.viewControllers lastObject];
    if ([detailVC respondsToSelector:@selector(setSplitViewBarButtonItem:)]) {
        [detailVC setSplitViewBarButtonItem:barButtonItem];
    }
}

- (void)splitViewController:(UISplitViewController *)svc
     willShowViewController:(UIViewController *)aViewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    id detailVC = [self.splitViewController.viewControllers lastObject];
    if ([detailVC respondsToSelector:@selector(setSplitViewBarButtonItem:)]) {
        [detailVC setSplitViewBarButtonItem:nil];
    }
}

@end
