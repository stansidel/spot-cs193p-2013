//
//  PhotoViewController.h
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController

@property (strong, nonatomic) NSURL *photoUrl;

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem;
@property (weak, nonatomic, readonly) UIBarButtonItem *splitViewBarButtonItem;

@end
