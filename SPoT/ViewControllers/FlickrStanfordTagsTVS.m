//
//  FlickrStanfordTagsTVS.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "FlickrStanfordTagsTVS.h"
#import "FlickrFetcher.h"

@interface FlickrStanfordTagsTVS ()

@end

@implementation FlickrStanfordTagsTVS

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadStanfordPhotos];
}

- (void)loadStanfordPhotos
{
    [self.refreshControl beginRefreshing];
    dispatch_queue_t fetcherQ = dispatch_queue_create("Stanford Photos Fetcher 4 tags", NULL);
    dispatch_async(fetcherQ, ^{
        NSArray *stanfordPhotos = [FlickrFetcher stanfordPhotos];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photos = stanfordPhotos;
            [self.refreshControl endRefreshing];
        });
    });
}

- (IBAction)refreshTableView:(UIRefreshControl *)sender {
    [self loadStanfordPhotos];
}

@end
