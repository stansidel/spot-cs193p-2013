//
//  PhotosForTagTVC.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 14/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "PhotosForTagTVC.h"
#import "FlickrFetcher.h"

@interface PhotosForTagTVC ()

@end

@implementation PhotosForTagTVC

- (BOOL)addViewedPhotosToRecent
{
    return YES;
}

- (void)setPhotosTag:(NSString *)photosTag
{
    _photosTag = photosTag;
    [self updateStanfordPhotos];
}

- (void)updateStanfordPhotos
{
    NSString *photosTag = self.photosTag;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@",
                              FLICKR_TAGS,
                              photosTag];
    dispatch_queue_t fetchStanfordPhotosQ = dispatch_queue_create("Stanford photos downloader", NULL);
    dispatch_async(fetchStanfordPhotosQ, ^{
        NSArray *allPhotos = [FlickrFetcher stanfordPhotos];
        NSArray *photos = [allPhotos filteredArrayUsingPredicate:predicate];
        if ([self.photosTag isEqualToString:photosTag]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.photos = [photos  sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:FLICKR_PHOTO_TITLE ascending:YES]]];
            });
        }
    });
}

@end
