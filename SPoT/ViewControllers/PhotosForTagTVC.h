//
//  PhotosForTagTVC.h
//  SPoT
//
//  Created by Stanislav Sidelnikov on 14/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "PhotosTVC.h"

@interface PhotosForTagTVC : PhotosTVC

@property (strong, nonatomic) NSString *photosTag;

@end
