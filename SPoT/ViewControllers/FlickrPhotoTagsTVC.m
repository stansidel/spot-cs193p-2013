//
//  FlickrPhotoTagsTVC.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "FlickrPhotoTagsTVC.h"
#import "FlickrFetcher.h"
#import "PhotosForTagTVC.h"

@interface FlickrPhotoTagsTVC ()

@property (strong, nonatomic, readonly) NSCountedSet *tagsPhotosCount;
@property (strong, nonatomic, readonly) NSOrderedSet *tags;

@end

@implementation FlickrPhotoTagsTVC

- (NSArray *)ignoredTags
{
    return @[@"cs193pspot", @"portrait", @"landscape"];
}

- (void)setTagsFromPhotos:(NSArray *)photos
{
    __block NSCountedSet *tagsCountedSet = [NSCountedSet new];
    __block NSMutableOrderedSet *tags = [NSMutableOrderedSet new];
    [photos enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
        if ([[obj valueForKey:FLICKR_TAGS] isKindOfClass:[NSString class]]) {
            NSString *photoTagsString = (NSString *)[obj valueForKey:FLICKR_TAGS];
            NSArray *photoTags = [photoTagsString componentsSeparatedByString:@" "];
            for (NSString *tag in photoTags) {
                if (![[self ignoredTags] containsObject:tag]) {
                    [tagsCountedSet addObject:tag];
                    [tags addObject:tag];
                }
            }
        }
    }];
    _tagsPhotosCount = tagsCountedSet;
    [tags sortUsingComparator:(NSComparator)^(id obj1, id obj2){
        return [obj1 localizedCaseInsensitiveCompare:obj2];
    }];
    _tags = tags;
}

- (void)setPhotos:(NSArray *)photos
{
    _photos = photos;
    [self setTagsFromPhotos:photos];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView reloadData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tags count];
}

- (NSString *)titleForRow:(NSUInteger)row
{
    return [[[self.tags objectAtIndex:row] description] capitalizedString];
}

- (NSString *)subtitleForRow:(NSUInteger)row
{
    NSUInteger count = [self.tagsPhotosCount countForObject:[self.tags objectAtIndex:row]];
    return [NSString stringWithFormat:@"Photos: %lu", (unsigned long)count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag" forIndexPath:indexPath];
    
    cell.textLabel.text = [self titleForRow:indexPath.row];
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if ([segue.identifier isEqualToString:@"Show Tag Photos"]) {
            if ([segue.destinationViewController respondsToSelector:@selector(setPhotosTag:)]) {
                [segue.destinationViewController performSelector:@selector(setPhotosTag:)
                                                      withObject:self.tags[indexPath.row]];
                [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
            }
        }
    }
}

@end
