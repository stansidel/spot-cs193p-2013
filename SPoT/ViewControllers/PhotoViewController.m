//
//  PhotoViewController.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "PhotoViewController.h"
#import "ImageCache.h"

@interface PhotoViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@property (nonatomic) BOOL userZoomedPhoto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *titleBarButtonItem;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@end

@implementation PhotoViewController

- (void)setTitle:(NSString *)title
{
    super.title = title;
    self.titleBarButtonItem.title = title;
}

- (void)setPhotoUrl:(NSURL *)photoUrl
{
    _photoUrl = photoUrl;
    [self resetPhoto];
}

- (void)resetPhoto
{
    if (self.scrollView) {
        self.scrollView.contentSize = CGSizeZero;
        self.imageView.image = nil;
        self.userZoomedPhoto = NO;
        
        [self.spinner startAnimating];
        dispatch_queue_t imageFetchQ = dispatch_queue_create("Load image", NULL);
        dispatch_async(imageFetchQ, ^{
            NSData *imageData = [ImageCache getImageByURL:self.photoUrl];
            
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (image) {
                    self.scrollView.contentSize = image.size;
                    self.scrollView.zoomScale = 1.0;
                    self.imageView.image = image;
                    self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                    [self autoZoomPhoto];
                }
                [self.spinner stopAnimating];
            });
        });
    }
}

- (void)autoZoomPhoto
{
    CGRect visibleRect = [self.scrollView convertRect:self.scrollView.bounds toView:self.imageView];
    CGRect imageRect = self.imageView.bounds;
    if (imageRect.size.width == 0 || imageRect.size.height == 0
        || visibleRect.size.width == 0 || visibleRect.size.height == 0) {
        return;
    }
    CGFloat zoomHeight = visibleRect.size.height / imageRect.size.height;
    CGFloat zoomWidth = visibleRect.size.width / imageRect.size.width;
    // We need to fill the view completely with image so scale as much as possible
    CGFloat currentZoomScale = self.scrollView.zoomScale * MAX(zoomHeight, zoomWidth);
    if (currentZoomScale > self.scrollView.maximumZoomScale) {
        currentZoomScale = self.scrollView.maximumZoomScale;
    } else if (currentZoomScale < self.scrollView.minimumZoomScale) {
        currentZoomScale = self.scrollView.minimumZoomScale;
    }
    self.scrollView.zoomScale = currentZoomScale;
    // With change to the scrollView's zoomScale prop the scrollViewDidZoom:
    // event occures thus setting the userZoomedPhoto prop to YES. Fixing it
    self.userZoomedPhoto = NO;
}

- (UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    return _imageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    self.scrollView.minimumZoomScale = 0.2;
    self.scrollView.maximumZoomScale = 5.0;
    self.scrollView.delegate = self;
    [self resetPhoto];
    self.titleBarButtonItem.title = self.title;
    [self setSplitViewBarButtonItem:self.splitViewBarButtonItem];
}

- (void)viewDidLayoutSubviews
{
    if (!self.userZoomedPhoto) {
        [self autoZoomPhoto];
    }
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem) {
        [toolbarItems removeObject:_splitViewBarButtonItem];
    }
    if (barButtonItem) {
        [toolbarItems insertObject:barButtonItem atIndex:0];
    }
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = barButtonItem;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(nonnull UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidZoom:(nonnull UIScrollView *)scrollView
{
    self.userZoomedPhoto = YES;
}

@end
