//
//  PhotosTVC.h
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosTVC : UITableViewController

@property (strong, nonatomic) NSArray *photos;
@property (nonatomic, readonly) BOOL addViewedPhotosToRecent;

@end
