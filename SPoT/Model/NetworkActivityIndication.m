//
//  NetworkActivityIndication.m
//  SPoT
//
//  Created by stan on 20/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

@import UIKit;
#import "NetworkActivityIndication.h"

static NSUInteger activeProcessesCount = 0;

@implementation NetworkActivityIndication

+ (void)startIndicator
{
    activeProcessesCount++;
    UIApplication *application = [UIApplication sharedApplication];
    if (!application.isNetworkActivityIndicatorVisible) {
        application.networkActivityIndicatorVisible = YES;
    }
}

+ (void)stopIndicator
{
    activeProcessesCount--;
    if (activeProcessesCount == 0) {
        UIApplication *application = [UIApplication sharedApplication];
        if (application.isNetworkActivityIndicatorVisible) {
            application.networkActivityIndicatorVisible = NO;
        }
    }
}

@end
