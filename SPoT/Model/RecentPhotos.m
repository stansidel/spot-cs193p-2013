//
//  RecentPhoto.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 14/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "RecentPhotos.h"
#import "FlickrFetcher.h"

@interface RecentPhotos()

@end

@implementation RecentPhotos

#define ALL_PHOTOS_KEY @"RecentPhotos_All"

#define MAX_RECENT_PHOTOS 20

+ (NSArray *)allPhotos
{
    NSArray *allPhotos = [[NSUserDefaults standardUserDefaults] arrayForKey:ALL_PHOTOS_KEY];
    if (!allPhotos) allPhotos = [[NSArray alloc] init];
    return allPhotos;
}

+ (void)addPhoto:(NSDictionary *)photo
{
    NSMutableArray *allPhotos = [[self.class allPhotos] mutableCopy];
    
    NSPredicate *flickrIdPredicate = [NSPredicate predicateWithFormat:@"%K != %@", FLICKR_PHOTO_ID, photo[FLICKR_PHOTO_ID]];
    [allPhotos filterUsingPredicate:flickrIdPredicate];
    
    [allPhotos insertObject:photo atIndex:0];
    
    if ([allPhotos count] > MAX_RECENT_PHOTOS) {
        NSRange arrayTailRange;
        arrayTailRange.location = MAX_RECENT_PHOTOS;
        arrayTailRange.length = [allPhotos count] - MAX_RECENT_PHOTOS;
        [allPhotos removeObjectsInRange:arrayTailRange];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:allPhotos forKey:ALL_PHOTOS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
