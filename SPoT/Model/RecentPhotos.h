//
//  RecentPhoto.h
//  SPoT
//
//  Created by Stanislav Sidelnikov on 14/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentPhotos : NSObject

+ (NSArray *)allPhotos;
+ (void)addPhoto:(NSDictionary *)photo;

@end
