//
//  ImageCache.h
//  SPoT
//
//  Created by stan on 21/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlickrFetcher.h"

@interface ImageCache : NSObject

+ (NSData *)getImageByURL:(NSURL *)imageUrl;

@end
