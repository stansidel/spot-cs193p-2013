//
//  PhotosTVC.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 13/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "PhotosTVC.h"
#import "FlickrFetcher.h"
#import "PhotoViewController.h"
#import "RecentPhotos.h"
#import "TagsTabBarController.h"

@interface PhotosTVC ()

@end

@implementation PhotosTVC

- (BOOL)addViewedPhotosToRecent
{
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPhotos:(NSArray *)photos
{
    _photos = photos;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.photos count];
}

- (NSString *)titleForRow:(NSUInteger)row
{
    return [self.photos[row][FLICKR_PHOTO_TITLE] description];
}

- (NSString *)subtitleForRow:(NSUInteger)row
{
    return [[self.photos[row] valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo" forIndexPath:indexPath];
    
    cell.textLabel.text = [self titleForRow:indexPath.row];
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row];
    
    return cell;
}

#pragma mark - Navigation

- (NSURL *)urlForPhotoInRow:(NSUInteger)row
{
    return [FlickrFetcher urlForPhoto:self.photos[row] format:FlickrPhotoFormatLarge];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if ([segue.destinationViewController respondsToSelector:@selector(setPhotoUrl:)]) {
            NSURL *url = [self urlForPhotoInRow:indexPath.row];
            [segue.destinationViewController performSelector:@selector(setPhotoUrl:) withObject:url];
            [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
            
            if (self.addViewedPhotosToRecent) {
                [RecentPhotos addPhoto:self.photos[indexPath.row]];
            }
        }
    }
    
    if ([segue.identifier isEqualToString:@"Show Photo"]) {
        id tabBarController = self.tabBarController;
        if ([tabBarController respondsToSelector:@selector(transferSplitViewBarButtonItemToViewController:)]) {
            [tabBarController transferSplitViewBarButtonItemToViewController:segue.destinationViewController];
        }
    }
}

@end
