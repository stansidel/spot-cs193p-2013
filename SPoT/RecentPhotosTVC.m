//
//  RecentPhotosTVC.m
//  SPoT
//
//  Created by Stanislav Sidelnikov on 14/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "RecentPhotosTVC.h"
#import "RecentPhotos.h"
#import "FlickrFetcher.h"

@interface RecentPhotosTVC ()

@end

@implementation RecentPhotosTVC

- (void)setPhotosFromRecent
{
    NSArray *recentPhotos = [RecentPhotos allPhotos];
    self.photos = recentPhotos;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setPhotosFromRecent];
}

@end
